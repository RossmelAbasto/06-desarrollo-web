clear.addEventListener("click", () => 
{
    display.value = "";
});

deleteLast.addEventListener("click", () =>
{
    display.value = display.value.slice(0, -1);
});

divide.addEventListener("click", () =>
{
    display.value += "/";
});

seven.addEventListener("click", () =>
{
    display.value += "7";
});

eight.addEventListener("click", () =>
{
    display.value += "8";
});

nine.addEventListener("click", () =>
{
    display.value += "9";
});

multiply.addEventListener("click", () =>
{
    display.value += "*";
});

four.addEventListener("click", () =>
{
    display.value += "4";
});

five.addEventListener("click", () =>
{
    display.value += "5";
});

six.addEventListener("click", () =>
{
    display.value += "6";
});

minus.addEventListener("click", () =>
{
    display.value += "-";
});

one.addEventListener("click", () =>
{
    display.value += "1";
});

two.addEventListener("click", () =>
{
    display.value += "2";
});

three.addEventListener("click", () =>
{
    display.value += "3";
});

plus.addEventListener("click", () =>
{
    display.value += "+";
});

zero.addEventListener("click", () =>
{
    display.value += "0";
});

decimal.addEventListener("click", () =>
{
    display.value += ".";
});

equal.addEventListener("click", () =>
{
    display.value = evaluateResult(eval(display.value));
});


document.addEventListener("keydown", (event) =>
{
    if (event.key == "Escape")
    {
        display.value = "";
    }  
    
    if (event.key == "Backspace")
    {
        display.value = display.value.slice(0, -1);
    }
    
    if (event.key == "/")
    {
        display.value = display.value + "/";
    }

    if (event.key == "7")
    {
        display.value += "7";
    }
    
    if (event.key == "8")
    {
        display.value += "8";
    }

    if (event.key == "9")
    {
        display.value += "9";
    }

    if (event.key == "*")
    {
        display.value += "*";
    }

    if (event.key == "4")
    {
        display.value += "4";
    }

    if (event.key == "5")
    {
        display.value += "5";
    }

    if (event.key == "6")
    {
        display.value += "6";
    }

    if (event.key == "-")
    {
        display.value += "-";
    }

    if (event.key == "1")
    {
        display.value += "1";
    }

    if (event.key == "2")
    {
        display.value += "2";
    }

    if (event.key == "3")
    {
        display.value += "3";
    }

    if (event.key == "+")
    {
        display.value += "+";
    }

    if (event.key == "0")
    {
        display.value += "0";
    }

    if (event.key == ".")
    {
        display.value += ".";
    }

    if (event.key == "Enter")
    {
        display.value = evaluateResult(eval(display.value));
    }
});

function evaluateResult(result)
{
    if (result == "Infinity" || result == "NaN" || result == "undefined" || result == "null" || result == "")
    {
        return "error";
    }
    else
    {
        return result;
    }
}