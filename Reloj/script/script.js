function date()
{
    var fecha = new Date();
    var dia = fecha.getDate();
    var diaTexto = fecha.getDay();
    var mes = fecha.getMonth();
    var anio = fecha.getFullYear();

    var dias = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    
    document.getElementById("date").value = dias[diaTexto]+" "+dia+" de "+meses[mes]+" de "+anio;

    setTimeout("date()",500);
}

function time()
{
    var fecha = new Date();
    var hora = checkTime(fecha.getHours());
    var minutos = checkTime(fecha.getMinutes());
    var segundos = checkTime(fecha.getSeconds());

    ampm = (hora < 12) ? "AM" : "PM";
    hora = (hora == 0) ? 12 : hora;
    hora = (hora > 12) ? hora - 12 : hora;

    document.getElementById("time").value = hora+":"+minutos+":";
    document.getElementById("second").value = segundos;
    document.getElementById("ampm").value = ampm;

    setTimeout("time()",500);
}

function checkTime(number) {
    if (number < 10) {
        number = "0" + number;
    }
    return number;
}