const imgGallery = document.querySelectorAll('.imgGallery');
const changeImage = document.querySelector('.changeImage');
const imageContainer = document.querySelector('.imageView');

imgGallery.forEach(imagen => {
    imagen.addEventListener('click', () => {
        aparecerImagen(imagen.getAttribute('src'));
    });
});

const aparecerImagen = (imagen) => {
    changeImage.src = imagen;

    imageContainer.classList.add('show');
    changeImage.classList.add('showImage');
};


imageContainer.addEventListener('click', (e) => {
    if (e.target !== changeImage) {
        imageContainer.classList.remove('show');
        changeImage.classList.remove('showImage');
    }
});