function toogleMenu() 
{
    var icon = document.querySelector('.icon');
    if (icon.classList.contains('fa-bars'))
    {
        icon.classList.replace('fa-bars', 'fa-xmark');
    } else {
        icon.classList.replace('fa-xmark', 'fa-bars');
    }
    
    var menu_sidebar = document.querySelector('.menu-sidebar');

    if (menu_sidebar.classList.contains('show-bar')) 
    {
        menu_sidebar.classList.remove('show-bar');
    } 
    else 
    {
        menu_sidebar.classList.add('show-bar');
    }

}

function hideBar()
{
    var menu_sidebar = document.querySelector('.menu-sidebar');
    menu_sidebar.classList.remove('show-bar');

    var icon = document.querySelector('.icon');
    icon.classList.replace('fa-xmark', 'fa-bars');
}