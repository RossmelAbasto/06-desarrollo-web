const loading = document.querySelector(".loading");
const form = document.getElementById("form");
const result = document.getElementById("result");

form.addEventListener("submit", function (e) {
  const formData = new FormData(form);
  e.preventDefault();
  loading.classList.remove("hidden");

  result.style.display = "block";
  var object = {};
  formData.forEach((value, key) => {
    object[key] = value;
  });
  var json = JSON.stringify(object);
  result.innerHTML = "Enviando...";

  fetch("https://api.web3forms.com/submit", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: json,
  })
    .then(async (response) => {
      let json = await response.json();
      if (response.status == 200) {
        result.innerHTML = "Enviado";
      } else {
        console.log(response);
        result.innerHTML = "Error: " + json.message;
      }
    })
    .catch((error) => {
      console.log(error);
      result.innerHTML = "Algo salió mal";
    })
    .then(function () {
      form.reset();
      setTimeout(() => {
        result.style.display = "none";
        loading.classList.add("hidden");
      }, 2000);
    });
});
